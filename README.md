# README #

This app is a sample app to reproduce the -11819 error.

Through many experiments and data, it is estimated that -11819 occurs more frequently when AVVideoCompositionCoreAnimationTool is used and the resolution of the video to be exported is higher.

Occasionally, it appears as an error -11800 instead of -11819.

(AVVideoCompositionCoreAnimationTool is used to overlay text, images, etc. on the video.)


How to use is as follows.

1. After launching the app, select one of the videos scraped from the [Photos].
2. After setting the resolution and whether to use AVVideoCompositionCoreAnimationTool, click the [Export] button to start exporting. (Please set the resolution to 4K and with AVVideoCompositionCoreAnimationTool to get -11819 error.)
3. Below the [Export] button, progress and error messages are displayed.
4. When the export is completed, it is saved in the [Photos] app.