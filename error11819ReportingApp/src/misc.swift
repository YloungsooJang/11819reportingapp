//
//  misc.swift
//  error11819ReportingApp
//
//  Created by 장영수 on 2021/06/12.
//

import UIKit
import AVKit

enum SideAnchor {
  case top
  case bottom
  case left
  case right
}

extension UIView {

  func addSubviewByFilling(_ subview: UIView, edgeInsets: UIEdgeInsets = .zero) {
    addSubview(subview)

    subview.translatesAutoresizingMaskIntoConstraints = false
    subview.topAnchor.constraint(equalTo: topAnchor, constant: edgeInsets.top).isActive = true
    subview.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -edgeInsets.bottom).isActive = true
    subview.leftAnchor.constraint(equalTo: leftAnchor, constant: edgeInsets.left).isActive = true
    subview.rightAnchor.constraint(equalTo: rightAnchor, constant: -edgeInsets.right).isActive = true
  }

  func addSubviewAtSide(
    _ subview: UIView,
    viewWidth: CGFloat? = nil,
    at sideAnchor: SideAnchor = .bottom,
    safeArea: Bool = false
  ) {
    addSubview(subview)

    let topAnchor = safeArea ? safeAreaLayoutGuide.topAnchor : self.topAnchor
    let bottomAnchor = safeArea ? safeAreaLayoutGuide.bottomAnchor : self.bottomAnchor
    let leftAnchor = safeArea ? safeAreaLayoutGuide.leftAnchor : self.leftAnchor
    let rightAnchor = safeArea ? safeAreaLayoutGuide.rightAnchor : self.rightAnchor
    subview.translatesAutoresizingMaskIntoConstraints = false
    switch sideAnchor {
    case .top, .bottom:
      if let viewWidth = viewWidth {
        subview.heightAnchor.constraint(equalToConstant: viewWidth).isActive = true
      }
      subview.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
      subview.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
      if sideAnchor == .top {
        subview.topAnchor.constraint(equalTo: topAnchor).isActive = true
      } else {
        subview.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
      }
    case .left, .right:
      if let viewWidth = viewWidth {
        subview.widthAnchor.constraint(equalToConstant: viewWidth).isActive = true
      }
      subview.topAnchor.constraint(equalTo: topAnchor).isActive = true
      subview.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
      if sideAnchor == .left {
        subview.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
      } else {
        subview.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
      }
    }
  }

}

func getTimeFormatedString(_ duration: TimeInterval) -> String {
  let roundedDuration = duration.rounded()
  let formatter = DateComponentsFormatter()
  formatter.zeroFormattingBehavior = .pad

  if roundedDuration >= 3600 {
    formatter.allowedUnits = [.hour, .minute, .second]
  } else {
    formatter.allowedUnits = [.minute, .second]
  }

  return formatter.string(from: roundedDuration) ?? ""
}

extension CGFloat {

  func floored(nearest: CGFloat = 1) -> CGFloat {
    let intDiv = CGFloat(Int(self / nearest))
    return intDiv * nearest
  }

}

extension CGSize {

  var isPortrait: Bool {
    return height > width
  }

  func floored(nearest: CGFloat = 1) -> CGSize {
    return CGSize(
      width: width.floored(nearest: nearest),
      height: height.floored(nearest: nearest)
    )
  }

}

func getRectAspectFit(originalSize: CGSize, boundingSize: CGSize) -> CGRect {
  if originalSize.width == 0 || originalSize.height == 0 {
    return .zero
  }

  var aspectFitSize = boundingSize
  let widthRatio = boundingSize.width / originalSize.width
  let heightRatio = boundingSize.height / originalSize.height
  if heightRatio < widthRatio {
    aspectFitSize.width = heightRatio * originalSize.width
    return CGRect(
      origin: CGPoint(
        x: (boundingSize.width - aspectFitSize.width) / 2,
        y: 0
      ),
      size: aspectFitSize
    )
  }
  if widthRatio < heightRatio {
    aspectFitSize.height = widthRatio * originalSize.height
    return CGRect(
      origin: CGPoint(
        x: 0,
        y: (boundingSize.height - aspectFitSize.height) / 2
      ),
      size: aspectFitSize
    )
  }
  return CGRect(origin: CGPoint.zero, size: aspectFitSize)
}

extension AVAsset {

  var resolutionSize: CGSize? {
    guard let assetVideoTrack = tracks(
      withMediaType: AVMediaType.video
    ).first else { return nil }
    let size = assetVideoTrack.naturalSize.applying(
      assetVideoTrack.preferredTransform
    )
    let absoluteSize = CGSize(width: abs(size.width), height: abs(size.height))
    return absoluteSize.width > 0 && absoluteSize.height > 0 ? absoluteSize : nil
  }

}
