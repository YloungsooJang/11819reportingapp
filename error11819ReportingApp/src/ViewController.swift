//
//  ViewController.swift
//  error11819ReportingApp
//
//  Created by 장영수 on 2021/06/12.
//

import UIKit
import Photos

class ViewController: UIViewController {

  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .darkContent
  }

  private let label = UILabel()
  private var collectionView: UICollectionView?

  private var viewModels: [ViewModel] = [] {
    didSet {
      collectionView?.reloadData()
      collectionView?.setContentOffset(.zero, animated: false)
    }
  }

  private let columnCount: CGFloat = 3
  private let spacing: CGFloat = 4

  override func viewDidLoad() {
    super.viewDidLoad()

    setupView()
    checkAuthorization()
  }

  func fetchAssets() {
    print("fetchAssets")
    var viewModels: [ViewModel] = []
    DispatchQueue.global().async {
      let fetchAssets = PHAsset.fetchAssets(with: .video, options: nil)
      fetchAssets.enumerateObjects({ (phAsset, _, _) in
        viewModels.append(ViewModel(phAsset: phAsset))
      })

      DispatchQueue.main.async {
        self.viewModels = viewModels.reversed()
      }
    }
  }

  private func checkAuthorization() {
    let status = PHPhotoLibrary.authorizationStatus()
    if status == .authorized {
      DispatchQueue.main.async {
        self.fetchAssets()
      }
      return
    }

    if status == .notDetermined {
      PHPhotoLibrary.requestAuthorization { _ in
        self.checkAuthorization()
      }
      return
    }

    DispatchQueue.main.async {
      if let settingsURL = URL(string: UIApplication.openSettingsURLString) {
        UIApplication.shared.open(settingsURL, options: [:], completionHandler: nil)
      }
    }
  }

  private func setupView() {
    view.backgroundColor = UIColor.white
    setLabel()
    setCollectionView()
  }

  private func setLabel() {
    label.text = "choose video to export"
    label.textColor = UIColor.black
    label.font = UIFont.systemFont(ofSize: 15)
    label.textAlignment = .center
    label.numberOfLines = 0
    view.addSubview(label)

    label.translatesAutoresizingMaskIntoConstraints = false
    label.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 10).isActive = true
    label.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 10).isActive = true
    label.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10).isActive = true
  }

  private func setCollectionView() {
    let layout = UICollectionViewFlowLayout()
    layout.minimumInteritemSpacing = spacing
    layout.minimumLineSpacing = spacing

    let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
    collectionView.backgroundColor = nil
    collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 30, right: 0)
    view.addSubview(collectionView)

    collectionView.translatesAutoresizingMaskIntoConstraints = false
    collectionView.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 10).isActive = true
    collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    collectionView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 10).isActive = true
    collectionView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10).isActive = true

    collectionView.register(
      CollectionViewCell.self,
      forCellWithReuseIdentifier: CollectionViewCell.identifier
    )
    collectionView.dataSource = self
    collectionView.delegate = self

    self.collectionView = collectionView
  }

}

extension ViewController: UICollectionViewDataSource {

  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return viewModels.count
  }

  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(
      withReuseIdentifier: CollectionViewCell.identifier,
      for: indexPath
      ) as? CollectionViewCell

    let viewModel = viewModels[indexPath.item]
    cell?.setViewModel(viewModel)

    return cell!
  }

}

extension ViewController: UICollectionViewDelegateFlowLayout {

  func collectionView(
    _ collectionView: UICollectionView,
    layout collectionViewLayout: UICollectionViewLayout,
    sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
    let width = floor((collectionView.bounds.width - (columnCount - 1) * spacing) / columnCount)
    return CGSize(width: width, height: width)
  }

}

extension ViewController: UICollectionViewDelegate {

  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    let phAsset = viewModels[indexPath.item].phAsset
    let settingViewController = SettingViewController()
    settingViewController.phAsset = phAsset
    present(settingViewController, animated: true, completion: nil)
  }

}
