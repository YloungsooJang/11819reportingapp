//
//  SettingViewController.swift
//  error11819ReportingApp
//
//  Created by 장영수 on 2021/06/12.
//

import UIKit
import Photos

class SettingViewController: UIViewController {

  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .darkContent
  }

  var phAsset: PHAsset?

  private let headerBar = UIView()
  private let label = UILabel()
  private lazy var optionStackView = {
    return OptionStackView(titles: ["720p", "1080p", "4K"], checkedIndex: resolutionIndex)
  }()
  private let switchView = UIView()
  private let exportButton = UIButton()
  private let progressLabel = UILabel()

  private var exportSession: CoreExportSession?

  private var resolutionIndex = 2
  private var withCoreAnimationTool: Bool = true

  private let landscapeResolutionOptions: [CGSize] = [
    CGSize(width: 1280, height: 720),
    CGSize(width: 1920, height: 1080),
    CGSize(width: 3840, height: 2160)
  ]
  private let portraitResolutionOptions: [CGSize] = [
    CGSize(width: 720, height: 1280),
    CGSize(width: 1080, height: 1920),
    CGSize(width: 2160, height: 3840)
  ]

  override func viewDidLoad() {
    super.viewDidLoad()

    setupView()
  }

  // private

  private func setupView() {
    view.backgroundColor = UIColor.white
    setHeaderBar()
    setLabel()
    setOptionStackView()
    setSwitchView()
    setExportButton()
    setProgressLabel()
  }

  private func setHeaderBar() {
    view.addSubview(headerBar)

    headerBar.translatesAutoresizingMaskIntoConstraints = false
    headerBar.heightAnchor.constraint(equalToConstant: 59).isActive = true
    headerBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
    headerBar.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor).isActive = true
    headerBar.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor).isActive = true

    let backButton = UIButton()
    backButton.setTitle("back", for: .normal)
    backButton.setTitleColor(UIColor.black, for: .normal)
    headerBar.addSubviewAtSide(backButton, viewWidth: 59, at: .left)

    backButton.addTarget(self, action: #selector(backButtonPressed), for: .touchUpInside)
  }

  private func setLabel() {
    label.text = "choose video resolution and then press [Export] button"
    label.textColor = UIColor.black
    label.font = UIFont.systemFont(ofSize: 15)
    label.textAlignment = .center
    label.numberOfLines = 0
    view.addSubview(label)

    label.translatesAutoresizingMaskIntoConstraints = false
    label.topAnchor.constraint(equalTo: headerBar.bottomAnchor, constant: 10).isActive = true
    label.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 10).isActive = true
    label.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10).isActive = true
  }

  private func setOptionStackView() {
    view.addSubview(optionStackView)

    optionStackView.translatesAutoresizingMaskIntoConstraints = false
    optionStackView.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 10).isActive = true
    optionStackView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 10).isActive = true
    optionStackView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10).isActive = true

    optionStackView.delegate = self
  }

  private func setSwitchView() {
    view.addSubview(switchView)

    switchView.translatesAutoresizingMaskIntoConstraints = false
    switchView.topAnchor.constraint(equalTo: optionStackView.bottomAnchor, constant: 10).isActive = true
    switchView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 10).isActive = true
    switchView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10).isActive = true

    let methodSwitch = UISwitch()
    methodSwitch.isOn = withCoreAnimationTool
    methodSwitch.addTarget(self, action: #selector(onClickSwitch(sender:)), for: .valueChanged)
    switchView.addSubview(methodSwitch)

    methodSwitch.translatesAutoresizingMaskIntoConstraints = false
    methodSwitch.topAnchor.constraint(equalTo: switchView.topAnchor, constant: 10).isActive = true
    methodSwitch.bottomAnchor.constraint(equalTo: switchView.bottomAnchor, constant: -10).isActive = true
    methodSwitch.rightAnchor.constraint(equalTo: switchView.rightAnchor, constant: -10).isActive = true

    let titleLabel = UILabel()
    titleLabel.text = "with AVVideoCompositionCoreAnimationTool"
    titleLabel.textColor = UIColor.black
    titleLabel.font = UIFont.systemFont(ofSize: 13)
    titleLabel.numberOfLines = 0
    switchView.addSubview(titleLabel)

    titleLabel.translatesAutoresizingMaskIntoConstraints = false
    titleLabel.topAnchor.constraint(equalTo: switchView.topAnchor, constant: 10).isActive = true
    titleLabel.bottomAnchor.constraint(equalTo: switchView.bottomAnchor, constant: -10).isActive = true
    titleLabel.leftAnchor.constraint(equalTo: switchView.leftAnchor, constant: 10).isActive = true
    titleLabel.rightAnchor.constraint(lessThanOrEqualTo: methodSwitch.leftAnchor, constant: -10).isActive = true
  }

  private func setExportButton() {
    exportButton.layer.borderWidth = 1
    exportButton.layer.borderColor = UIColor.darkGray.cgColor
    exportButton.layer.cornerRadius = 5
    exportButton.setTitle("Export", for: .normal)
    exportButton.setTitleColor(UIColor.black, for: .normal)
    view.addSubview(exportButton)

    exportButton.translatesAutoresizingMaskIntoConstraints = false
    exportButton.topAnchor.constraint(equalTo: switchView.bottomAnchor, constant: 20).isActive = true
    exportButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    exportButton.widthAnchor.constraint(equalToConstant: 150).isActive = true
    exportButton.heightAnchor.constraint(equalToConstant: 60).isActive = true

    exportButton.addTarget(self, action: #selector(exportButtonPressed), for: .touchUpInside)
  }

  private func setProgressLabel() {
    progressLabel.textColor = UIColor.black
    progressLabel.font = UIFont.systemFont(ofSize: 15)
    progressLabel.textAlignment = .center
    progressLabel.numberOfLines = 0
    view.addSubview(progressLabel)

    progressLabel.translatesAutoresizingMaskIntoConstraints = false
    progressLabel.topAnchor.constraint(equalTo: exportButton.bottomAnchor, constant: 10).isActive = true
    progressLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 10).isActive = true
    progressLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10).isActive = true
  }

  @objc private func backButtonPressed() {
    exportSession?.cancel()
    if let viewController = presentingViewController as? ViewController {
      viewController.fetchAssets()
    }
    dismiss(animated: true, completion: nil)
  }

  @objc func onClickSwitch(sender: UISwitch) {
    self.withCoreAnimationTool = sender.isOn
  }

  @objc private func exportButtonPressed() {
    if let phAsset = phAsset {
      let options = PHVideoRequestOptions()
      options.isNetworkAccessAllowed = true
      options.deliveryMode = .highQualityFormat

      progressLabel.text = "exporting..."
      PHImageManager.default().requestAVAsset(forVideo: phAsset, options: options) { (avAsset, _, _) in
        DispatchQueue.main.async {
          if let avAsset = avAsset, let assetVideoSize = avAsset.resolutionSize {
            let videoSize = self.limitExportedVideoSize(
              assetVideoSize,
              resolutionIndex: self.resolutionIndex
            )
            print("originalVideoSize: \(assetVideoSize), videoSizetoExport: \(videoSize)")

            let compositions = AssetComposition(avAsset: avAsset, videoSize: videoSize).getCompositions(self.withCoreAnimationTool)

            self.exportSession = CoreExportSession(
              avAsset: compositions.composition,
              videoSize: videoSize,
              videoComposition: compositions.videoComposition
            )
            self.exportSession?.delegate = self
            self.exportSession?.export()
          }
        }
      }
    }
  }

  private func limitExportedVideoSize(_ size: CGSize, resolutionIndex: Int) -> CGSize {
    let resolutionOptions = size.isPortrait ? portraitResolutionOptions : landscapeResolutionOptions
    let targetResolution = resolutionOptions[resolutionIndex]
    return getRectAspectFit(
      originalSize: size,
      boundingSize: targetResolution
    ).size.floored(nearest: 2)
  }

}

extension SettingViewController: OptionStackViewDelegate {

  func chooseOption(index: Int) {
    self.resolutionIndex = index
  }

}

extension SettingViewController: CoreExportSessionDelegate {

  func coreExportSessionCompletedHandler() {
    DispatchQueue.main.async {
      self.progressLabel.text = "exporting is finished. Please check your [Photos] app."
      self.exportSession = nil
    }
  }

  func coreExportSessionProgressHandler(progress: Float) {
    DispatchQueue.main.async {
      self.progressLabel.text = "exporting... \(Int(progress * 100))%"
    }
  }

  func coreExportSessionFailedHandler(error: CoreExportSessionError, description: String?) {
    DispatchQueue.main.async {
      self.progressLabel.text = description
    }
  }

}
