//
//  OptionStackView.swift
//  error11819ReportingApp
//
//  Created by 장영수 on 2021/06/12.
//

import UIKit

protocol OptionStackViewDelegate: AnyObject {
  func chooseOption(index: Int)
}

class OptionStackView: UIView {

  weak var delegate: OptionStackViewDelegate?

  private let titles: [String]
  private var checkedIndex: Int {
    didSet {
      if checkedIndex != oldValue
        && checkedIndex >= 0 && checkedIndex < stackView.arrangedSubviews.count {
        delegate?.chooseOption(index: checkedIndex)
        stackView.arrangedSubviews.enumerated().forEach { index, subview in
          if let optionItem = subview as? OptionItem {
            optionItem.setChecked(index == checkedIndex)
          }
        }
      }
    }
  }

  private let stackView = UIStackView()

  init(titles: [String], checkedIndex: Int) {
    self.titles = titles
    self.checkedIndex = checkedIndex
    super.init(frame: .zero)

    setupView()
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  private func setupView() {
    translatesAutoresizingMaskIntoConstraints = false
    heightAnchor.constraint(equalToConstant: 49).isActive = true

    setStackView()
  }

  private func setStackView() {
    stackView.spacing = 8
    stackView.distribution = .fillEqually
    addSubviewByFilling(stackView)

    setItems()
  }

  private func setItems() {
    titles.enumerated().forEach { index, title in
      let optionItem = OptionItem(
        title: title,
        index: index,
        checked: index == checkedIndex
      )

      optionItem.addGestureRecognizer(UITapGestureRecognizer(
        target: self,
        action: #selector(tapOption(sender:)))
      )

      stackView.addArrangedSubview(optionItem)
    }
  }

  @objc private func tapOption(sender: UITapGestureRecognizer) {
    if let optionItem = sender.view as? OptionItem {
      self.checkedIndex = optionItem.index
    }
  }

}

class OptionItem: UIView {

  private let title: String
  let index: Int
  private var checked: Bool {
    didSet {
      UIView.animate(withDuration: 0.15) {
        self.backgroundColor = self.checked ? UIColor.systemGreen : nil
      }
    }
  }

  private let label = UILabel()

  init(title: String, index: Int, checked: Bool) {
    self.title = title
    self.index = index
    self.checked = checked
    super.init(frame: .zero)

    setupView()
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func setChecked(_ checked: Bool) {
    self.checked = checked
  }

  private func setupView() {
    backgroundColor = checked ? UIColor.systemGreen : nil
    layer.borderWidth = 1
    layer.borderColor = UIColor.gray.cgColor
    layer.cornerRadius = 5

    setLabel()
  }

  private func setLabel() {
    label.text = title
    label.font = UIFont.systemFont(ofSize: 15)
    label.textColor = UIColor.black
    label.allowsDefaultTighteningForTruncation = true
    label.textAlignment = .center
    addSubviewByFilling(
      label,
      edgeInsets: UIEdgeInsets(
        top: 0,
        left: 16,
        bottom: 0,
        right: 16
      )
    )
  }

}
