//
//  CoreExportSession.swift
//  error11819ReportingApp
//
//  Created by 장영수 on 2021/06/12.
//

import AVKit
import Photos

enum CoreExportSessionError: String {
  case exportFailed
  case unknownStatus
  case setupFailure
  case readingFailure
  case writingFailure
}

protocol CoreExportSessionDelegate: AnyObject {
  func coreExportSessionCompletedHandler()
  func coreExportSessionProgressHandler(progress: Float)
  func coreExportSessionFailedHandler(error: CoreExportSessionError, description: String?)
}

class CoreExportSession {

  weak var delegate: CoreExportSessionDelegate?

  private var avAsset: AVAsset
  private var videoSize: CGSize
  private var videoComposition: AVMutableVideoComposition?

  private var exporter: NextLevelSessionExporter?
  private var progress: Float = 0 {
    didSet {
      if progress != oldValue {
        delegate?.coreExportSessionProgressHandler(progress: progress)
      }
    }
  }

  private let outputURL: URL = FileManager.default.temporaryDirectory.appendingPathComponent("File from error11819ReportingApp.mov")

  init(
    avAsset: AVAsset,
    videoSize: CGSize,
    videoComposition: AVMutableVideoComposition?
  ) {
    self.avAsset = avAsset
    self.videoSize = videoSize
    self.videoComposition = videoComposition
  }

  // public

  func export() {
    // delete old exported file
    try? FileManager.default.removeItem(at: outputURL)
    initExportSession()
    log("start exporting")
    exporter?.export(
      progressHandler: { self.progress = $0 },
      completionHandler: exportDidFinishExporter
    )
  }

  func cancel() {
    log("cancelExport")
    exporter?.cancelExport()
  }

  // private

  private func initExportSession() {
    let availablePresets = AVAssetExportSession.exportPresets(compatibleWith: avAsset)

    let exporter = NextLevelSessionExporter(withAsset: avAsset)
    exporter.outputFileType = .mov
    exporter.outputURL = outputURL
    exporter.optimizeForNetworkUse = true
    exporter.videoComposition = videoComposition
    self.exporter = exporter

    exporter.videoOutputConfiguration = [
      AVVideoCodecKey: availablePresets.contains(AVAssetExportPresetHEVCHighestQuality)
        ? AVVideoCodecType.hevc
        : AVVideoCodecType.h264,
      AVVideoWidthKey: NSNumber(value: Int(videoSize.width)),
      AVVideoHeightKey: NSNumber(value: Int(videoSize.height))
    ]
    exporter.audioOutputConfiguration = [
      AVFormatIDKey: kAudioFormatMPEG4AAC,
      AVNumberOfChannelsKey: NSNumber(value: 2),
      AVSampleRateKey: NSNumber(value: Float(44100))
    ]
  }

  private func exportDidFinishExporter(result: Swift.Result<AVAssetExportSession.Status, Error>) {
    self.exporter = nil

    var error: CoreExportSessionError?
    var description: String?

    switch result {
    case .success(let status):
      switch status {
      case .completed:
        PHPhotoLibrary.shared().performChanges({
          PHAssetChangeRequest.creationRequestForAssetFromVideo(
            atFileURL: self.outputURL
          )
        }, completionHandler: { success, error in
          if let nserror = error as NSError? {
            print(nserror.debugDescription)
          }
          self.delegate?.coreExportSessionCompletedHandler()
        })
        return
      default:
        log("success status: \(status.rawValue)")
        error = .unknownStatus
      }
    case .failure(let error_):
      if let nextLevelError = error_ as? NextLevelSessionExporterError {
        switch nextLevelError {
        case .setupFailure:
          error = .setupFailure
        case .readingFailure:
          error = .readingFailure
        case .writingFailure:
          error = .writingFailure
        case .cancelled:
          log("cancelled")
          return
        }
      }
      if error != nil {
        break
      }

      error = .exportFailed
      if let nserror = error_ as NSError? {
        log(nserror.debugDescription)
        description = nserror.debugDescription
      }
    }

    if let error = error {
      log("\(error) | \(description ?? "")")
      delegate?.coreExportSessionFailedHandler(error: error, description: description)
    }
  }

  private func log(_ message: String) {
    print("[CoreExportSession] \(message)")
  }

}
