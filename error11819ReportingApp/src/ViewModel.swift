//
//  ViewModel.swift
//  error11819ReportingApp
//
//  Created by 장영수 on 2021/06/12.
//

import UIKit
import Photos

class ViewModel {

  let phAsset: PHAsset
  var thumbnail: UIImage?

  private var requestImageID: Int = 0

  init(phAsset: PHAsset) {
    self.phAsset = phAsset
  }

  func fetchThumbnail(size: CGSize, completion: @escaping (UIImage?) -> Void) {
    guard thumbnail == nil else {
      DispatchQueue.main.async {
        completion(self.thumbnail)
      }
      return
    }

    if requestImageID != 0 {
      PHImageManager.default().cancelImageRequest(PHImageRequestID(requestImageID))
    }

    let options = PHImageRequestOptions()
    options.isNetworkAccessAllowed = true
    options.deliveryMode = .opportunistic
    options.resizeMode = .fast

    let id = PHImageManager.default().requestImage(
      for: phAsset,
      targetSize: CGSize(width: size.width * 2, height: size.height * 2),
      contentMode: .aspectFill,
      options: options,
      resultHandler: { (image, _) -> Void in
        self.thumbnail = image
        DispatchQueue.main.async {
          completion(self.thumbnail)
        }
      }
    )

    self.requestImageID = Int(id)
  }

}
