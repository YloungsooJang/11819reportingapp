//
//  AssetComposition.swift
//  error11819ReportingApp
//
//  Created by 장영수 on 2021/06/12.
//

import AVKit

struct AssetCompositionResult {
  let composition: AVMutableComposition
  let videoComposition: AVMutableVideoComposition?
}

class AssetComposition {

  private var avAsset: AVAsset
  private var videoSize: CGSize

  private var videoTrack: AVMutableCompositionTrack?

  private let frameRate: CMTimeScale = 60

  init(
    avAsset: AVAsset,
    videoSize: CGSize
  ) {
    self.avAsset = avAsset
    self.videoSize = videoSize
  }

  // public

  func getCompositions(_ withCoreAnimationTool: Bool) -> AssetCompositionResult {
    let composition = AVMutableComposition()
    self.videoTrack = composition.addMutableTrack(
      withMediaType: AVMediaType.video,
      preferredTrackID: kCMPersistentTrackID_Invalid
    )
    let audioTrack = composition.addMutableTrack(
      withMediaType: AVMediaType.audio,
      preferredTrackID: kCMPersistentTrackID_Invalid
    )

    let assetVideoTrack = avAsset.tracks(withMediaType: AVMediaType.video).first
    let assetAudioTrack = avAsset.tracks(withMediaType: AVMediaType.audio).first
    let timeRange = CMTimeRangeMake(start: CMTime.zero, duration: avAsset.duration)
    let atTime = CMTime.zero
    do {
      if let assetVideoTrack = assetVideoTrack {
        try videoTrack?.insertTimeRange(
          timeRange,
          of: assetVideoTrack,
          at: atTime
        )
      }
      if let assetAudioTrack = assetAudioTrack {
        try audioTrack?.insertTimeRange(
          timeRange,
          of: assetAudioTrack,
          at: atTime
        )
      }
    } catch {
    }

    return AssetCompositionResult(
      composition: composition,
      videoComposition: getVideoComposition(composition: composition, withCoreAnimationTool: withCoreAnimationTool)
    )
  }

  // private

  private func getVideoComposition(composition: AVMutableComposition, withCoreAnimationTool: Bool) -> AVMutableVideoComposition {
    let videoComposition = AVMutableVideoComposition(propertiesOf: composition)
    videoComposition.renderSize = videoSize
    videoComposition.frameDuration = CMTime(
      value: 1,
      timescale: frameRate
    )
    videoComposition.sourceTrackIDForFrameTiming = kCMPersistentTrackID_Invalid
    videoComposition.instructions = videoTrack != nil ? [getInstruction(timeRange: CMTimeRange(start: .zero, duration: composition.duration))] : []
    videoComposition.animationTool = withCoreAnimationTool ? getAVVideoCompositionCoreAnimationTool() : nil

    return videoComposition
  }

  private func getInstruction(
    timeRange: CMTimeRange
  ) -> AVMutableVideoCompositionInstruction {
    let mainInstruction = AVMutableVideoCompositionInstruction()
    mainInstruction.timeRange = timeRange

    let layerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: videoTrack!)
    layerInstruction.setTransform(
      getSrcSpecificTransform(),
      at: CMTime.zero
    )

    mainInstruction.layerInstructions = [layerInstruction]
    return mainInstruction
  }

  private func getSrcSpecificTransform() -> CGAffineTransform {
    let currentVideoSize = avAsset.resolutionSize!
    let widthRatio = videoSize.width / currentVideoSize.width
    let heightRatio = videoSize.height / currentVideoSize.height
    let scaleValue = min(widthRatio, heightRatio)
    let assetVideoTrack = avAsset.tracks(withMediaType: AVMediaType.video).first
    let transform = assetVideoTrack?.preferredTransform ?? CGAffineTransform.identity

    let targetRect = getRectAspectFit(originalSize: currentVideoSize, boundingSize: videoSize)
    return getTransform(
      transform.scaledBy(x: scaleValue, y: scaleValue),
      targetRect
    )
  }

  private func getTransform(
    _ assetTransform: CGAffineTransform,
    _ targetRect: CGRect
  ) -> CGAffineTransform {
    let a = assetTransform.a
    let b = assetTransform.b
    let c = assetTransform.c
    let d = assetTransform.d

    let portrait = b > 0 && c < 0
    let portraitUpsideDown = b < 0 && c > 0
    let portraitFlipVertical = b < 0 && c < 0

    let landscapeLeft = a < 0 && d < 0
    let landscapeRightFlipHorizontal = a < 0 && d > 0
    let landscapeRightFlipVertical = a > 0 && d < 0

    let tx = portrait || landscapeLeft || landscapeRightFlipHorizontal || portraitFlipVertical
      ? targetRect.width
      : 0
    let ty = portraitUpsideDown || landscapeLeft || landscapeRightFlipVertical || portraitFlipVertical
      ? targetRect.height
      : 0
    return CGAffineTransform(
      a: a,
      b: b,
      c: c,
      d: d,
      tx: tx + targetRect.origin.x,
      ty: ty + targetRect.origin.y
    )
  }

  private func getAVVideoCompositionCoreAnimationTool() -> AVVideoCompositionCoreAnimationTool {
    let parentLayer = CALayer()
    parentLayer.frame = CGRect(origin: CGPoint.zero, size: videoSize)

    let videoLayer = CALayer()
    videoLayer.frame = CGRect(origin: CGPoint.zero, size: videoSize)
    parentLayer.addSublayer(videoLayer)

    return AVVideoCompositionCoreAnimationTool(
      postProcessingAsVideoLayer: videoLayer,
      in: parentLayer
    )
  }

}
